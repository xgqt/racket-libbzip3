# Racket-libbzip3

FFI to libbzip3.


## About

bzip3 project upstream: https://github.com/kspalaiologos/bzip3


## License

Copyright (c) 2022, Maciej Barć <xgqt@riseup.net>

Licensed under the GNU GPL v3 License

SPDX-License-Identifier: GPL-3.0-or-later
